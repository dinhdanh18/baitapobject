var dsnv = []

//* LOCALSTORAGE
function luuVaoLocal(dsnv){
    dsnvJSON = JSON.stringify(dsnv);
    localStorage.setItem("dsnv",dsnvJSON);
}
var JSONdata = localStorage.getItem("dsnv");
if(JSONdata !== null){
    var arrayDSNV = JSON.parse(JSONdata);
    for(i = 0 ; i < arrayDSNV.length ; i++){
        var item = arrayDSNV[i];
        var nv = new NhanVien(item.tk, item.ten, item.email, item.mk, item.ngayLam, item.luong, item.chucVu, item.gioLam) 
        dsnv.push(nv);
    }
    renderDSNV(dsnv)
}
//*HIỂN THỊ DANH SÁCH NHÂN VIÊN
function renderDSNV(arrayDSNV){
    var content = "";
    for ( i = 0; i < arrayDSNV.length ; i++){
        var nv = arrayDSNV[i];
        var contentTR = 
        `<tr>
            <td>${nv.tk}</td>
            <td>${nv.ten}</td>                
            <td>${nv.email}</td>                
            <td>${nv.ngayLam}</td>                
            <td>${nv.chucVu}</td>                
            <td>${nv.tongLuong()}</td>                
            <td>${nv.xepLoai()}</td>  
            <td class="d-flex">
            <button class="btn btn-success mr-1" data-toggle="modal" data-target="#myModal" onclick="layThongTinNV(${nv.tk})">Sửa</button>
            <button class="btn btn-danger"onclick="xoaNhanVien(${nv.tk})">Xóa</button>
        </td>              
        </tr>`
        content = content + contentTR;
    }
    getElement('tableDanhSach').innerHTML = content;
}
//* THÊM NHÂN VIÊN
function themNhanVien(){
getElement("tbTKNV").style.display = "block";
getElement("tbTen").style.display = "block";
getElement("tbMatKhau").style.display = "block";
getElement("tbEmail").style.display = "block";
getElement("tbNgay").style.display = "block";
getElement("tbLuongCB").style.display = "block";
getElement("tbChucVu").style.display = "block";
getElement("tbGiolam").style.display = "block";

    var nv = layThongTinTuForm();
    var valid = true;
    valid &= kiemTraRong(nv.tk,"#tbTKNV","Tài khoản")
    & kiemTraRong(nv.ten,"#tbTen","Tên")
    & kiemTraRong(nv.mk,"#tbMatKhau","Mật khẩu")
    & kiemTraRong(nv.email,"#tbEmail","Email")
    & kiemTraRong(nv.ngayLam,"#tbNgay","Ngày làm")
    & kiemTraRong(nv.luong,"#tbLuongCB","Lương")
    & kiemTraRong(nv.chucVu,"#tbChucVu","Chức vụ")
    & kiemTraRong(nv.gioLam,"#tbGiolam","Giờ làm");

    valid &= kiemTraKySo(nv.tk,"#tbTKNV","Tài khoản")
    & kiemTraKySo(nv.luong,"#tbLuongCB","Lương")
    & kiemTraKySo(nv.gioLam,"#tbGiolam","Giờ làm");
    
    valid &= kiemTraMatKhau(nv.mk,"#tbMatKhau","Mật Khẩu")
    valid &= kiemTraTatCaKyTu(nv.ten,"#tbTen","Tên");
    
    valid &= kiemTraEmail(nv.email,"#tbEmail","Email");

    valid &= kiemTraGioLam(nv.gioLam,"#tbGiolam", "Giờ làm");
    valid &= kiemTraLuong(nv.luong,"#tbLuongCB", "Lương cơ bản");
    valid &= kiemTraTK(nv.tk,"#tbTKNV","Tài khoản",dsnv,nv.tk);
    valid &= kiemTraChucVu(nv.chucVu,"#tbChucVu","Chức vụ");
    valid &= kiemTraNgay(nv.ngayLam,"#tbNgay","Ngày làm");
    
    if(!valid){
       return;
    }
    dsnv.push(nv);
    renderDSNV(dsnv);
    luuVaoLocal(dsnv);
    resetInput();
}
//* XÓA NHÂN VIÊN
function xoaNhanVien(tkNV){
    var viTri = timKiemViTri(dsnv,tkNV);
    if(viTri != -1){
        var sv = dsnv[viTri];
        dsnv.splice(viTri,1);
        renderDSNV(dsnv);
        luuVaoLocal(dsnv);
    }    
}
function layThongTinNV(tkNV){
    var viTri = timKiemViTri(dsnv,tkNV);
    if(viTri != -1){
        var nv = dsnv[viTri]
        hienThiThongTinLenForm(nv);
        getElement("tknv").disabled = true;

    }
}
//* CẬP NHẬT NHÂN VIÊN
function capNhatNhanVien(){
    getElement("tknv").disabled = false;
    var nv = layThongTinTuForm();
    var viTri = timKiemViTri(dsnv, nv.tk);
    dsnv[viTri] = nv;
    renderDSNV(dsnv);
    luuVaoLocal(dsnv);
    resetInput();
}
//* TÌM KIẾM NHÂN VIÊN THEO XẾP LOẠI
document.getElementById("searchName").onkeyup = function (e) {
    var value = e.target.value;
    var data = TimKiemNhanVien(dsnv, value);
   renderDSNV(data);
}