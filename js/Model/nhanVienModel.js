var NhanVien = function (_tk, _ten, _email, _mk, _ngayLam, _luong, _chucVu,_gioLam ){
    this.tk = _tk;
    this.ten = _ten;
    this.email = _email;
    this.mk = _mk;
    this.ngayLam = _ngayLam;
    this.luong = _luong;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;
    this.tongLuong = function(){
        if(this.chucVu == "Sếp"){
            return this.luong * 3;
        }else if(this.chucVu == "Trưởng phòng"){
            return this.luong * 2;
        }else {
            return this.luong;
        }
    };
    this.xepLoai = function(){
        if (this.gioLam >= 192){
           return "Nhân Viên Xuất Sắc";
        }else if(this.gioLam >= 176){
           return "Nhân Viên Giỏi";
        }else if(this.gioLam >= 160){
            return "Nhân Viên Khá";
        }else {
            return "Nhân Viên Trung Bình";
        }
    }
};
