/**
 * Hàm kiểm tra rỗng dựa vào giá trị người dùng nhập vào
 * @param {*} value value là giá trị người dùng nhập
 * @param {*} selectorError selector hiển thị lỗi cho giá trị đó
 * @param {*} name tên thuộc tính bị lỗi khi hiển thị
 * @returns trả về giá trị hợp lệ(true) hoặc không hợp lệ(false)
 */
function kiemTraRong(value,selectorError,name){
    if(value ===''){
        document.querySelector(selectorError).innerHTML= name + " không được bỏ trống!";
        return false
    }
        document.querySelector(selectorError).innerHTML= `<span class="text-success">Hợp lệ</span>`;
        return true;
}
/**
 * Hàm kiểm tra giá trị người dùng nhập vào phải là ký tự
 * @param {*} value value là giá trị người dùng nhập
 * @param {*} selectorError selector hiển thị lỗi cho giá trị đó
 * @param {*} name tên thuộc tính bị lỗi khi hiển thị
 * @returns trả về giá trị hợp lệ(true) hoặc không hợp lệ(false)
 */
function kiemTraTatCaKyTu(value,selectorError,name){
    var regexLetter = /^[A-Z a-z]+$/; // nhập các ký tự a->z A->Z hoặc khoảng trống k bao gồm unicode
    if(regexLetter.test(value)){
        document.querySelector(selectorError).innerHTML= `<span class="text-success">Hợp lệ</span>`;
        return true;
    }
    document.querySelector(selectorError).innerHTML = name + " phải là chữ cái, không dấu!"
    return false;
}
/**
 * Hàm kiểm tra giá trị người dùng nhập vào phải là email
 * @param {*} value value là giá trị người dùng nhập
 * @param {*} selectorError selector hiển thị lỗi cho giá trị đó
 * @param {*} name tên thuộc tính bị lỗi khi hiển thị
 * @returns trả về giá trị hợp lệ(true) hoặc không hợp lệ(false)
 */
function kiemTraEmail(value,selectorError,name){
    var regexEmail =   /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regexEmail.test(value)){
        document.querySelector(selectorError).innerHTML= `<span class="text-success">Hợp lệ</span>`;
        return true;
    }
        document.querySelector(selectorError).innerHTML = name + ' không đúng định dạng';
    return false;
}
/**
 * Hàm kiểm tra giá trị người dùng nhập vào phải là ký số
 * @param {*} value value là giá trị người dùng nhập
 * @param {*} selectorError selector hiển thị lỗi cho giá trị đó
 * @param {*} name tên thuộc tính bị lỗi khi hiển thị
 * @returns trả về giá trị hợp lệ(true) hoặc không hợp lệ(false)
 */
function kiemTraKySo(value,selectorError,name){
    var regexNumber = /^[0-9]+$/;
    if(regexNumber.test(value)){
        document.querySelector(selectorError).innerHTML= `<span class="text-success">Hợp lệ</span>`;
        return true;
    }
    document.querySelector(selectorError).innerHTML = name + " phải là chữ số!"
    return false;
}
/**
 * Hàm kiểm tra giá trị người dùng nhập vào phải là ngày có định dạnh MM/DD/YYYY
 * @param {*} value value là giá trị người dùng nhập
 * @param {*} selectorError selector hiển thị lỗi cho giá trị đó
 * @param {*} name tên thuộc tính bị lỗi khi hiển thị
 * @returns trả về giá trị hợp lệ(true) hoặc không hợp lệ(false)
 */
function kiemTraNgay(value,selectorError,name){
    var reg = /^\d{2}\/\d{2}\/\d{4}$/;
    if(reg.test(value)){
        document.querySelector(selectorError).innerHTML= `<span class="text-success">Hợp lệ</span>`;
        return true;
    }
    document.querySelector(selectorError).innerHTML = name + " không hợp lệ 'MM/DD/YYYY'"
    return false;
}

//
function kiemTraTK(value,selectorError,name,dsnv,tk){
    var viTri = timKiemViTri(dsnv,tk);
    if(viTri !== -1){
        document.querySelector(selectorError).innerHTML = name + " đã tồn tại"
        return false;
    }
    if(value.length < 4 || value.length > 6){
    document.querySelector(selectorError).innerHTML = name + " tối đa 4 - 6 ký số"
    return false;
    }
    document.querySelector(selectorError).innerHTML= `<span class="text-success">Hợp lệ</span>`;
    return true;
}
function kiemTraGioLam(value,selectorError,name){
    if(value < 80 || value > 200){
    document.querySelector(selectorError).innerHTML = name + " tối đa 80 - 200 giờ 1 tháng"
    return false;
    }
    document.querySelector(selectorError).innerHTML= `<span class="text-success">Hợp lệ</span>`;
    return true;
}
function kiemTraLuong(value,selectorError,name){
    if(value < 1000 || value > 20000){
    document.querySelector(selectorError).innerHTML = name + " 1000$ - 20000$"
    return false;
    }
    document.querySelector(selectorError).innerHTML= `<span class="text-success">Hợp lệ</span>`;
    return true;
}
function kiemTraChucVu(value,selectorError,name){
    if(value == 0){
        document.querySelector(selectorError).innerHTML = name + " không hợp lệ"
        return false;
        }
        document.querySelector(selectorError).innerHTML= `<span class="text-success">Hợp lệ</span>`;
        return true;
}
/**
 * Hàm kiểm mật khẩu phải gồm  từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt, 1 ký tự thường)
 * @param {*} value value là giá trị người dùng nhập
 * @param {*} selectorError selector hiển thị lỗi cho giá trị đó
 * @param {*} name tên thuộc tính bị lỗi khi hiển thị
 * @returns trả về giá trị hợp lệ(true) hoặc không hợp lệ(false)
 */
function kiemTraMatKhau(value,selectorError,name){
    var reg =/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{6,10}$/;
    if(reg.test(value)){
        document.querySelector(selectorError).innerHTML= `<span class="text-success">Hợp lệ</span>`;
        return true;
    }
    document.querySelector(selectorError).innerHTML = name + " từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt, 1 ký tự thường)"
    return false;

}