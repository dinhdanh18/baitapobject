function getElement(element){
    return document.getElementById(element);
}
function layThongTinTuForm(){
    var tk = getElement("tknv").value;
    var ten = getElement("name").value;
    var email = getElement("email").value;
    var mk = getElement("password").value;
    var ngayLam = getElement("datepicker").value;
    var luong = getElement("luongCB").value;
    var chucVu = getElement("chucvu").value;
    var gioLam = getElement("gioLam").value;
    
    var nv = new NhanVien(tk, ten,email,mk,ngayLam,luong,chucVu,gioLam);
    return nv;
}
function timKiemViTri(dsnv, tkNV){
    return dsnv.findIndex(function(item){
        return item.tk == tkNV;
    });
}

function hienThiThongTinLenForm(nv){
     getElement("tknv").value = nv.tk;
     getElement("name").value = nv.ten;
     getElement("email").value = nv.email;
     getElement("password").value = nv.mk;
     getElement("datepicker").value = nv.ngayLam;
     getElement("luongCB").value = nv.luong;
     getElement("chucvu").value = nv.chucVu;
     getElement("gioLam").value = nv.gioLam;
    
}
function resetInput(){
     getElement("tknv").value = "";
     getElement("name").value = "";
     getElement("email").value = "";
     getElement("password").value = "";
     getElement("luongCB").value = "";
     getElement("chucvu").value = "";
     getElement("gioLam").value = "";
}
function TimKiemNhanVien(dsnv, value) {
    var arr = [];
    for (var i = 0; i < dsnv.length; i++) {
        value = value.toLowerCase();
        var tenXepLoai = dsnv[i].xepLoai().toLowerCase();
        if (tenXepLoai.includes(value)) {
            arr.push(dsnv[i]);
        }
    }
    return arr;
}